
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentManager {

    private List<Student> students;

    public StudentManager() {
        students = new ArrayList<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }


    public void inputStudent(){
        boolean tiepTucNhapSinhVien = true ;

        Scanner scanner = new Scanner(System.in);

        while (tiepTucNhapSinhVien){
            Student student = new Student();
            System.out.print("Nhập tên sinh viên: ");
            student.setNameStudent(scanner.nextLine());
            System.out.print("Nhập mã sinh viên: ");
            student.setIdStudent(scanner.nextInt());
            scanner.nextLine();
            System.out.print("Nhập  điểm toán: ");
            student.setPointMath(scanner.nextDouble());
            scanner.nextLine();
            System.out.print("Nhập điểm tiếng Anh: ");
            student.setPointEnglish(scanner.nextDouble());
            scanner.nextLine();
            System.out.print("Nhập mã lớp: ");
            student.setClassStudent(scanner.nextLine());

            addStudent(student);

            System.out.print(" Bạn có muoons tiếp tục nhập ? (Y/N)");
            String select = scanner.nextLine();

            if (select.equalsIgnoreCase("N")) {
                tiepTucNhapSinhVien = false; // Ngắt vòng lặp khi người dùng chọn "N"
            }
        }
    }

    public void showStudent(){

        if (students.isEmpty()){
            System.out.println(" Chưa có sinh viên được nhập !!");
        }
        else {
            System.out.println("Danh Sách Sinh Viên");
            for (Student student : students){
                System.out.println("Tên: " + student.getNameStudent() +
                        ", Mã sinh viên: " + student.getIdStudent() +
                        ", Điểm toán: " + student.getPointMath() +
                        ", Điểm tiếng Anh: " + student.getPointEnglish() +
                        ", Lớp: " + student.getClassStudent());
            }
        }
    }
}

/// test commit
