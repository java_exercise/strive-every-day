public class Student {
    private String nameStudent ;
    private int idStudent;
    private double pointEnglish;
    private double pointMath;

    private String classStudent;

    public Student (){};

    public Student(String nameStudent, double pointEnglish, double pointMath, int idStudent, String classStudent) {
        this.nameStudent = nameStudent;
        this.pointEnglish = pointEnglish;
        this.pointMath = pointMath;
        this.idStudent = idStudent;
        this.classStudent = classStudent;
    }

    public String getNameStudent() {
        return nameStudent;
    }

    public void setNameStudent(String nameStudent) {
        this.nameStudent = nameStudent;
    }

    public double getPointEnglish() {
        return pointEnglish;
    }

    public void setPointEnglish(double pointEnglish) {
        this.pointEnglish = pointEnglish;
    }

    public double getPointMath() {
        return pointMath;
    }

    public void setPointMath(double pointMath) {
        this.pointMath = pointMath;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getClassStudent() {
        return classStudent;
    }

    public void setClassStudent(String classStudent) {
        this.classStudent = classStudent;
    }


    // add sinh vien

    
}
