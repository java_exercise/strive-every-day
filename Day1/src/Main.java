import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        StudentManager studentManager = new StudentManager();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Chọn menu: ");
            int menu = scanner.nextInt();
            switch (menu) {

                case 1:
                    studentManager.inputStudent();
                    break;

                case 2:
                    studentManager.showStudent();
                    break;
            }
        }
    }
}